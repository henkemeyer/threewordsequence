import unittest
from solution import get_three_word_sequences
from solution import lookahead


class SolutionTest(unittest.TestCase):

    def test_EmptyList(self):
        my_dict = get_three_word_sequences([])
        assert(len(my_dict) == 0)

    def test_OneWord(self):
        my_dict = get_three_word_sequences(['only'])
        assert(len(my_dict) == 0)

    def test_TwoWords(self):
        my_dict = get_three_word_sequences(['this', 'that'])
        assert(len(my_dict) == 0)

    def test_OneSequence(self):
        my_dict = get_three_word_sequences(['one', 'two', 'three'])
        assert(my_dict['one two three'] == 1)
        assert(len(my_dict) == 1)

    def test_FourIdenticalWords(self):
        my_dict = get_three_word_sequences(['yes', 'yes', 'yes', 'yes'])
        assert(my_dict['yes yes yes'] == 2)
        assert(len(my_dict) == 1)

    def test_OverlappingSequences(self):
        my_dict = get_three_word_sequences(['this', 'here', 'this', 'here', 'this'])
        assert(my_dict['this here this'] == 2)
        assert(my_dict['here this here'] == 1)
        assert(len(my_dict) == 2)

    def test_MixedCase(self):
        my_dict = get_three_word_sequences(['Look', 'at', 'me', 'look', 'at', 'ME'])
        assert(my_dict['look at me'] == 2)
        assert(len(my_dict) == 3)

# Note: after writing this unit test, it occurred to me that passing in less than 2 for the lookahead function makes no
# sense, so instead, I will write an assert in the lookahead function itself.
#
#    def testSingleLookahead(self):
#        my_list = ['one', 'two', 'three']
#        index = 0
#        for item in lookahead(my_list, 1):
#            if index == 0:
#                assert (item == 'one')
#            if index == 1:
#                assert (item == 'two')
#            if index == 2:
#                assert (item == 'three')
#            index += 1
#        assert index == 3

    def test_DoubleLookahead(self):
        my_list = ['one', 'two', 'three']
        index = 0
        for item1, item2 in lookahead(my_list, 2):
            if index == 0:
                assert (item1 == 'one')
                assert (item2 == 'two')
            if index == 1:
                assert (item1 == 'two')
                assert (item2 == 'three')
            index += 1
        assert index == 2

    def test_TripleLookahead(self):
        my_list = ['one', 'two', 'three']
        index = 0
        for item1, item2, item3 in lookahead(my_list, 3):
            if index == 0:
                assert (item1 == 'one')
                assert (item2 == 'two')
                assert (item3 == 'three')
            index += 1
        assert index == 1


if __name__ == '__main__':
    unittest.main()
