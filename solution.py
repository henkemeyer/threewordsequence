import sys
import select
import re
from itertools import tee
from collections import defaultdict
import time


# This is the lookahead function for when we iterate through the list of words
# You pass in the list (iterable), as well as how many items to look ahead (size)
def lookahead(iterable, size):
    assert size > 1, "The lookahead function only makes sense with a size of 2 or greater"
    iters = tee(iterable, size)
    for i in range(1, size):
        for each in iters[i:]:
            next(each, None)
    return zip(*iters)


# This function will take a list of words (input_list), and return a dictionary that
# shows the frequency of each unique three word sequence
def get_three_word_sequences(input_list):
    # Create an empty dictionary, and set the default value to 0
    my_dict = {}
    my_dict = defaultdict(lambda: 0, my_dict)

    # Iterate through the list of words, looking ahead two words as we go
    for word1, word2, word3 in lookahead(input_list, 3):
        three_words_sequence = "{0} {1} {2}".format(word1.lower(), word2.lower(), word3.lower())
        # Now stash the 3 words into a dict, and increment it's value by one
        my_dict[three_words_sequence] = my_dict[three_words_sequence] + 1

    return my_dict


def main():
    cmd_line = False
    entire_file = ""
    # First check to see if there is any data coming in from stdin
    if select.select([sys.stdin, ], [], [], 0.0)[0]:
        # Have to do it this way to prevent \n characters from getting into my strings
        entire_file = sys.stdin.read()
        num_files = 1

    if entire_file == "":
        # Now check the command line
        if len(sys.argv) < 2:
            print("Usage: python solution.py <filename>.txt")
            exit(1)
        if sys.argv[1] == '-h' or sys.argv[1] == "--help" or sys.argv[1] == "/h":
            print("Usage: python solution.py <filename>.txt")
            exit(1)
        num_files = len(sys.argv) - 1
        cmd_line = True

    for x in range(1, num_files+1):
        # Read the file into a large string
        if cmd_line:
            with open(sys.argv[x]) as fp:
                entire_file = fp.read()

        # Now strip out all punctuation (except apostrophes), and stuff the words into a list
        entire_file_sans_punctuation_list = re.sub(u"[^\w\d'\s]+", '', str(entire_file)).split()

        D = get_three_word_sequences(entire_file_sans_punctuation_list)

        # Now print out the sequences, ordered by the value
        sequence_counter = 0
        if cmd_line and len(sys.argv) > 1:
            print("\n" + sys.argv[x] + ":\n")
        for w in sorted(D, key=D.get, reverse=True):
            sequence_counter += 1
            if sequence_counter <= 100:
                print("{0} - {1}".format(w, D[w]))


if __name__ == "__main__":
    t0 = time.time()
    main()
    t1 = time.time()
    print("\n\nTotal Execution time : {0}".format(t1 - t0))
