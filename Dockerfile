FROM python:3
ADD solution.py /
ADD dracula.txt /
ADD tom_sawyer.txt /
ADD war_and_peace.txt /
ADD moby_dick.txt /
ADD six_able_men.txt /
CMD [ "python", "./solution.py", "./dracula.txt", "./tom_sawyer.txt", "./war_and_peace.txt", "./moby_dick.txt", "./six_able_men.txt" ]
