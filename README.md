# ThreeWordSequence

This is a programming challenge to read an input text file (or multiple, if passed in on the command line), and output a list of three word sequences, ordered by how many times the sequences appear.

See below for the requirements of the challenge.

**To run this solution**

Download python 3.7.x, install it, then execute from the command line:

*python solution.py \<textfile1\> \<textfile2\> ...*

Alternatively, you can pass a file in via stdin, as such:

*cat dracula.txt | python solution.py*

Alternatively, you can build a docker container, and run it in the container:

To build: *docker build -t solution .* (executed from project's root directory, which is the same directory as the Dockerfile)

To run: *docker run solution*

**Developer notes:**

*  Probably I will use a straight up iterator (but I'll need to implement a lookahead function so I can process three words per iteration).  I thought maybe a regular expression might be an option, but I prefer the readability of an iterator-based solution.
*  The plan is to store the three word sequences in a dictionary, using the three word sequence as the key, and incrementing the value of the dictionary entry by one each time I encounter a sequence.
*  Punctuation will have to be handled carefully.  Likely, I'll just strip out all characters that are not a-z, A-Z, or 0-9.  I should also keep apostophes.
*  Hyphens may prove to be problematic 

**What would I do next, given more time:**
* Make it multi-threaded to handle all input files in parallel DONE (on separate branch)
* Handle hyphens, underscores, (tm), (r), etc
* Test on linux, windows (this was developed on MacOS).  Something tells me that piping files in will have to be handled slightly differently by different operating systems
* Add support for Docker.  DONE
* But why stop there?  Bring up a Kubernetes cluster on AWS, implement a REST API, then integrate with Project Gutenburg! :)
* "Aggregate mode" - allow multiple books (or just input files in general) to be processed together, instead of separately.
* Implement this in Ruby, Golang, then interpret the differences in coding, performance, etc

**Known bugs:**
*  Trademark symbols aren't handled (so something that says "Gutenburg Project(tm)" ends up being interpreted as "Gutenburg Projecttm")
*  Underscores aren't handled
*  Hyphens aren't handled
*  Need to write unicode unit test


**The challenge:**

Please create a program executable from the command line that when given text(s) will return a list of the 100 most common three word sequences.

For example, if I ran ruby ./solution.rb moby_dick.txt the first lines of the result would be:

* the sperm whale - 85
* the white whale - 71
* of the whale - 67


**What to prioritize**

* We suggest spending 2 hours meeting the basic requirements and, if you so choose, 2 hours on the extra credit.
* Basic requirements should be met.
* Basic requirements should be tested.
* Code should be well structured. Consider extensibility. Consider readability.
* Your README should include 
* ** how to run your program. 
* ** What you would do next, given more time (if anything)? 
* ** Are there bugs that you are aware of?

**Basic Requirements**

* The program accepts as arguments a list of one or more file paths (e.g. ./solution.rb file1.txt file2.txt ...).
* The program also accepts input on stdin (e.g. cat file1.txt | ./solution.rb).
* The program outputs a list of the 100 most common three word sequences.
* The program ignores punctuation, line endings, and is case insensitive (e.g. “I love\nsandwiches.” should be treated the same as "(I LOVE SANDWICHES!!)"). Watch out that contractions aren't changed into 2 words (eg. shouldn't should not become shouldn t).
* The program should be tested. Provide a test file for your solution.
* The program should be well structured and understandable.
* The program is capable of processing large files and runs as fast as possible.

**Extra Credit**

* The program can run in a docker container.
* The program is capable of processing large files and remains performant. Think about if the program needed to handle 1,000 Moby Dick's at once. What would you need to do? Consider memory consumption and speed.
* It handles unicode characters(eg. the ü in Süsse or ß in Straße).


